import { SafeAreaView } from "react-native";
import React, { useEffect } from "react";
import * as Animatable from "react-native-animatable";
import * as Progress from "react-native-progress";
import { useNavigation } from "@react-navigation/core";

export default function PreparingOrderScreen() {
  const navigation = useNavigation();

  useEffect(() => {
    setTimeout(() => {
      navigation.navigate("Delivery");
    }, 4000);
  }, []);

  return (
    <SafeAreaView className="bg-[#00CCBB] flex-1 justify-center items-center">
      <Animatable.Image
        source={require("../assets/PreparingOrder.gif")}
        animation="slideInUp"
        iterationCount={1}
        className="w-11/12 h-3/6 aspect-auto rounded-full"
      />

      <Animatable.Text
        animation="slideInUp"
        iterationCount={1}
        className="text-white text-lg font-bold my-10 text-center"
      >
        Preparing your order
      </Animatable.Text>

      <Progress.Circle size={60} indeterminate={true} color="white" />
    </SafeAreaView>
  );
}
