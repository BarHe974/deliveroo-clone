import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import client from "./sanity";

export const getRestaurants = (id) => {
  const { data: restaurants } = useQuery(["restaurants"], () =>
    client
      .fetch(
        `
  *[_type == "featured" && _id == $id]
  {
      ...,
      restaurants[]->{
          ...,
          dishes[]->,
          type->{
              name
          }
      },
  }[0]
  `,
        { id }
      )
      .then((data) => {
        console.log("data:", data);
        return data?.restaurants;
      })
  );
  return restaurants;
};

export const getFeaturedCategories = () => {
  const { data: featuredCategories } = useQuery(["featuredCategories"], () =>
    client
      .fetch(
        `
    *[_type == 'featured'] {
        ...,
        restaurants[]->{
            ...,
            dishes[]->
        }
    }
    `
      )
      .then((data) => {
        return data;
      })
  );
  return featuredCategories;
};

export const getCategories = () => {
  const { data: categories } = useQuery(["categories"], () =>
    client.fetch(`*[_type == 'category']`).then((data) => {
      return data;
    })
  );
  return categories;
};

export const useRestaurant = () => ({
  queryKey: ["restaurant"],
  queryFn: async () => {
    return {
      id: null,
      imgUrl: null,
      title: null,
      rating: null,
      genre: null,
      address: null,
      short_description: null,
      dishes: null,
    };
  },
});

export const getRestaurantSelected = () => {
  const qc = useQueryClient();
  return qc.getQueryData(["restaurant"]);
};

export const updateRestaurant = () => {
  const qc = useQueryClient();
  return useMutation((restaurant) => {
    qc.setQueryData(["restaurant"], restaurant);
  });
};

export const useBasket = () => ({
  queryKey: ["basket"],
  queryFn: () => [],
});
export const useAddArticleOnBasket = () => {
  const qc = useQueryClient();
  return useMutation((article) => {
    qc.setQueryData(["basket"], [...qc.getQueryData(["basket"]), article]);
  });
};
export const useGroupArticlesById = () => {
  const qc = useQueryClient();
  const dataGroupedItems = qc
    .getQueryData(["basket"])
    .reduce((results, item) => {
      (results[item.id] = results[item.id] || []).push(item);
      return results;
    }, {});
  return dataGroupedItems;
};

export const useDeleteOneArticleFromBasket = () => {
  const qc = useQueryClient();
  return useMutation(({ id }) => {
    const idQty = qc.getQueryData(["basket"]).filter((a) => a.id === id).length;
    if (idQty > 1) {
      const articles = qc
        .getQueryData(["basket"])
        .filter((article) => article.id === id);
      const articleFiltered = qc
        .getQueryData(["basket"])
        .filter((article) => article.id !== id);
      const articlesToAdd = Array.from(
        { length: idQty - 1 },
        () => articles[0]
      );
      const articlesEdited = [...articleFiltered, ...articlesToAdd];
      qc.setQueryData(["basket"], articlesEdited);
    } else {
      qc.setQueryData(
        ["basket"],
        qc.getQueryData(["basket"]).filter((article) => article.id !== id)
      );
    }
  });
};

export const useRemoveArticleFromBasket = () => {
  const qc = useQueryClient();
  return useMutation((id) => {
    qc.setQueryData(
      ["basket"],
      qc.getQueryData(["basket"]).filter((article) => article.id !== id)
    );
  });
};

export const useGetAllArticlesFromBasket = () => {
  const qc = useQueryClient();
  return qc.getQueryData(["basket"]) ?? [];
};

export const useGetAllArticlesFromBasketWithId = (id) => {
  const qc = useQueryClient();
  return (
    qc.getQueryData(["basket"])?.filter((article) => article.id === id) ?? []
  );
};

export const useGetTotalPrice = () => {
  const qc = useQueryClient();
  return (
    qc
      .getQueryData(["basket"])
      ?.reduce((acc, article) => acc + article.price, 0) ?? 0
  );
};
