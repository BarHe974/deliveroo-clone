import { ScrollView } from "react-native";
import React from "react";
import CategoryCard from "./CategoryCard";
import { getCategories } from "../Queries";

const Categories = () => {
  const categories = getCategories();

  return (
    <ScrollView
      horizontal
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        paddingHorizontal: 15,
        paddingTop: 10,
      }}
    >
      {categories?.map((category) => (
        <CategoryCard
          key={category._id}
          imgUrl={category.image}
          title={category.name}
        />
      ))}
    </ScrollView>
  );
};

export default Categories;
