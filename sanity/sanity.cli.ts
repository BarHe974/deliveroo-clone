import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'wgfc8nzz',
    dataset: 'production'
  }
})
