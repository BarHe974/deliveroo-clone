export default {
  name: 'restaurant',
  type: 'document',
  title: 'Restaurant',
  fields: [
    {name: 'name', type: 'string', title: 'Name', validation: (Rule) => Rule.required()},
    {
      name: 'short_description',
      type: 'string',
      title: 'Short Description',
      validation: (Rule) => Rule.max(20),
    },
    {name: 'image', type: 'image', title: 'Image of the restaurant'},
    {name: 'address', type: 'string', title: 'Address', validation: (Rule) => Rule.required()},
    {name: 'long', type: 'number', title: 'Longitude'},
    {name: 'lat', type: 'number', title: 'Latitude'},
    {
      name: 'rating',
      type: 'number',
      title: 'Enter a rating frtom 1 to 5',
      validation: (Rule) => Rule.min(1).max(5).error('Rating must be between 1 and 5'),
    },
    {
      name: 'dishes',
      type: 'array',
      title: 'Dishes',
      of: [{type: 'reference', to: [{type: 'dish'}]}],
    },
    {
      name: 'type',
      title: 'Category',
      validation: (Rule) => Rule.required(),
      type: 'reference',
      to: [{type: 'category'}],
    },
  ],
}
