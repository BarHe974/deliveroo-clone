export default {
  name: 'dish',
  type: 'document',
  title: 'Dish',
  fields: [
    {name: 'name', type: 'string', title: 'Dish name', validation: (Rule) => Rule.required()},
    {
      name: 'short_description',
      type: 'string',
      title: 'Short Description',
      validation: (Rule) => Rule.max(200),
    },
    {name: 'image', type: 'image', title: 'Image of the dish'},
    {name: 'price', type: 'number', title: 'Price'},
  ],
}
